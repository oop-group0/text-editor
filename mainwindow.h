#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTextEdit>
#include <QFont>
#include <QColorDialog>
#include <QFileDialog>
#include <QTextStream>
//#include <QFileSystemWatcher>
#include <QMessageBox>
//#include <QTextCodec>
#include <QDir>
#include <QFontComboBox>
#include <QToolButton>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QActionGroup>
class QLineEdit;
class QDialog;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void createMenuBar();
    void createCentral();
    void createToolBar();
    void createDocuBar();
    void createlineedit();
    void SetTextFormat(QTextCharFormat fmt);

    QTextEdit *text;
    QHBoxLayout *layout;
    QToolBar *fontToolBar;
    QActionGroup *docu;
private:
    Ui::MainWindow *ui;

    QAction *openAct;
    QAction *closeAct;
    QAction *findAct;
    QAction *newAct;
    QAction *createAct1;

    QAction *undoAct;
    QAction *redoAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *deleteAct;

    QLabel *fontLabel1;
    QFontComboBox *fontComboBox;//字体

    QLabel *fontLabel2;
    QComboBox *sizeComboBox;    //字号

    QToolButton *boldBtn;       //加粗按钮
    QToolButton *italicBtn;     //斜体按钮
    QToolButton *underlineBtn;  //下划线按钮
    QToolButton *colorBtn;      //颜色按钮
    //QToolButton *findbtn; //

    QAction *leftAction;        //左对齐
    QAction *rightAction;       //右对齐
    QAction *centerAction;      //居中对齐
    QAction *balanceAction;     //两端对齐

    QLineEdit *findLineEdit;
    QLineEdit *replacelineedit;
    QDialog *findDlg;
    QDialog *createDlg; //新建的窗口

    QString opfilename;
 //   QFileSystemWatcher *pFileWatcher;

protected slots: //槽函数
    void SetFontComboBox(QString Combo);//设置字体
    void SetSizeComboBox(QString Value);//设置字号
    void SetBold();                     //加粗设置
    void SetItalic();                   //斜体设置
    void SetUnderline();                //下划线设置
    void SetColor();                    //颜色设置
    void SetAlignment(QAction *act);    //段落设置
    void onCursorPositionChanged();

    void showFindText();//查找文字
    void showFindText2();
    void on_action_Find_triggered();
    void on_actionOpen_triggered();//文件打开
    void on_actionSave_triggered();//文件另存为
    void on_actionSave1_triggered();//文件保存
    void on_actionNew_triggered(); //文件新建
    void replacetext();//替换文字
    void refresht();//将文件显示为已保存的状态
    void refresha();//将所有东西还原成原始状态
    bool canBeSaveOrNot();//文件是否能被保存
    void on_actionCreateAct1_triggered(); // 新窗口

    void on_actionundo_triggered();
    void on_actionredo_triggered();
    void on_actioncut_triggered() ;
    void on_actioncopy_triggered();
    void on_actionpatse_triggered();
    void on_actiondelete_triggered();
};
#endif // MAINWINDOW_H
