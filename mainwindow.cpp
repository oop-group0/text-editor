#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QToolBar>
#include<QLabel>
#include<QFontComboBox>
#include<QPushButton>
#include<QToolButton>
#include<QTextEdit>
#include<QStatusBar>
#include<QDateTime>
#include<QTimer>
#include<QFont>
#include<QFontComboBox>
#include<QToolButton>
#include<QFormLayout>
#include<QVBoxLayout>
#include<QScrollArea>
#include<QScrollBar>
#include<QDebug>
#include<QActionGroup>
#include<qlineedit.h>
#include<qdialog.h>
#include<qpushbutton.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("未命名-Text Editor");
    this->setWindowIcon(QIcon(":/img/text.png"));
    createCentral();
    createMenuBar();
    createToolBar();
    createDocuBar();

}

void MainWindow::createMenuBar(){ //创建菜单
    QMenuBar *Bar = this->menuBar();

    QMenu *filemenu = Bar->addMenu("文件(F)");
    /*关于文件的动作的添加和实现*/
    openAct = filemenu->addAction("打开");
    openAct->setShortcut(tr("Ctrl+O"));
    openAct->setStatusTip(QStringLiteral("打开一个文件"));
    connect(openAct,SIGNAL(triggered()),this,SLOT(on_actionOpen_triggered()));

    closeAct = filemenu->addAction("另存为");
    closeAct->setShortcut(tr("Ctrl+Shift+S"));
    closeAct->setStatusTip(QStringLiteral("保存一个文件"));
    connect(closeAct,SIGNAL(triggered()),this,SLOT(on_actionSave_triggered()));

    closeAct = filemenu->addAction("保存");
    closeAct->setShortcut(tr("Ctrl+S"));
    closeAct->setStatusTip(QStringLiteral("保存一个文件"));
    connect(closeAct,SIGNAL(triggered()),this,SLOT(on_actionSave1_triggered()));

    newAct = filemenu->addAction("新建");
    newAct->setShortcut(tr("Ctrl+N"));
    connect(newAct,SIGNAL(triggered()),this,SLOT(on_actionNew_triggered()));

    createAct1 = filemenu->addAction("新窗口");
    createAct1->setShortcut(tr("Ctrl+Shift+N"));
    connect(createAct1,SIGNAL(triggered()),this,SLOT(on_actionCreateAct1_triggered()));

    QMenu *editmenu = Bar->addMenu("编辑(E)");
    /*关于编辑的动作的添加和实现*/
    findAct = editmenu->addAction("查找与替换");
    findAct->setShortcut(tr("Ctrl+F"));
    connect(findAct,SIGNAL(triggered()),this,SLOT(on_action_Find_triggered()));

    undoAct = editmenu->addAction("撤销");
    undoAct->setShortcut(tr("Ctrl+Z"));
    connect(undoAct,SIGNAL(triggered()),this,SLOT(on_actionundo_triggered()));

    redoAct = editmenu->addAction("重做");
    redoAct->setShortcut(tr("Ctrl+Y"));
    connect(redoAct,SIGNAL(triggered()),this,SLOT(on_actionredo_triggered()));

    cutAct = editmenu->addAction("剪切");
    cutAct->setShortcut(tr("Ctrl+X"));
    connect(cutAct,SIGNAL(triggered()),this,SLOT(on_actioncut_triggered()));

    copyAct = editmenu->addAction("复制");
    copyAct->setShortcut(tr("Ctrl+C"));
    connect(copyAct,SIGNAL(triggered()),this,SLOT(on_actioncopy_triggered()));

    pasteAct = editmenu->addAction("粘贴");
    pasteAct->setShortcut(tr("Ctrl+V"));
    connect(pasteAct,SIGNAL(triggered()),this,SLOT(on_actionpaste_triggered()));

    deleteAct = editmenu->addAction("删除");
    deleteAct->setShortcut(tr("del"));
    connect(deleteAct,SIGNAL(triggered()),this,SLOT(on_actiondelete_triggered()));

    //QMenu *formmenu = Bar->addMenu("格式(O)");
    /*关于格式的动作的添加和实现*/


    //QMenu *viewmenu = Bar->addMenu("查看(V)");
    /*关于查看的动作的添加和实现*/
}


void MainWindow::createToolBar(){ //创建工具栏
    fontLabel1 = new QLabel(tr("字体"));
    fontComboBox = new QFontComboBox;
    fontComboBox->setFontFilters(QFontComboBox::ScalableFonts); //表示所有字体

    fontLabel2 = new QLabel(tr("字号"));
    sizeComboBox = new QComboBox;
    QFontDatabase db;
    foreach(int size, db.standardSizes()){
        sizeComboBox->addItem(QString::number(size)); //设置所有可行的字号
    }

    boldBtn = new QToolButton;
    boldBtn->setIcon(QIcon(":/img/bold.png"));
    boldBtn->setCheckable(true);
    boldBtn->setToolTip("加粗");

    italicBtn = new QToolButton;
    italicBtn->setIcon(QIcon(":/img/italic.png"));
    italicBtn->setCheckable(true);
    italicBtn->setToolTip("斜体");

    underlineBtn = new QToolButton;
    underlineBtn->setIcon(QIcon(":/img/underline.png"));
    underlineBtn->setCheckable(true);
    underlineBtn->setToolTip("下划线");

    colorBtn = new QToolButton;
    colorBtn->setIcon(QIcon(":/img/color.png"));
    colorBtn->setToolTip("字体颜色");

   /* findbtn=new QToolButton;
    findbtn->setIcon(QIcon(":/img/edit-find"));
    findbtn->setToolTip("查找");*/

    fontToolBar = this->addToolBar("Font");
    fontToolBar->addWidget(fontLabel1);
    fontToolBar->addWidget(fontComboBox);

    fontToolBar->addWidget(fontLabel2);
    fontToolBar->addWidget(sizeComboBox);

    fontToolBar->addSeparator();

    fontToolBar->addWidget(boldBtn);
    fontToolBar->addWidget(italicBtn);
    fontToolBar->addWidget(underlineBtn);
    fontToolBar->addSeparator();
    fontToolBar->addWidget(colorBtn);
    //fontToolBar->addWidget(findbtn);

    QFont font = text->font();
    QString fontFamily = font.family();
    int fontSize = font.pointSize();
    fontComboBox->setCurrentFont(font);
    sizeComboBox->setCurrentText(QString::number(fontSize)); //设置默认字体字号

    //设置事件关联
    connect(fontComboBox,SIGNAL(activated(QString)),this,SLOT(SetFontComboBox(QString)));
    connect(sizeComboBox,SIGNAL(activated(QString)),this,SLOT(SetSizeComboBox(QString)));
    connect(boldBtn,SIGNAL(clicked()),this,SLOT(SetBold()));
    connect(italicBtn,SIGNAL(clicked()),this,SLOT(SetItalic()));
    connect(underlineBtn,SIGNAL(clicked()),this,SLOT(SetUnderline()));
    connect(colorBtn,SIGNAL(clicked()),this,SLOT(SetColor()));
    //connect(findbtn,SIGNAL(clicked()),this,SLOT(on_action_Find_triggered()));
    //connect(findbtn,SIGNAL(clicked()),this,SLOT(showFindText()));

    connect(text, &QTextEdit::cursorPositionChanged, this, &MainWindow::onCursorPositionChanged);

}

void MainWindow::createDocuBar(){
    docu = new QActionGroup(this);
    leftAction = new QAction(QIcon(":/img/left.png"), "左对齐", docu);
    leftAction->setCheckable(true);
    rightAction = new QAction(QIcon(":/img/right.png"), "右对齐", docu);
    rightAction->setCheckable(true);
    centerAction = new QAction(QIcon(":/img/center.png"), "居中", docu);
    centerAction->setCheckable(true);
    balanceAction = new QAction(QIcon(":/img/balance.png"), "两端对齐", docu);
    balanceAction->setCheckable(true);

    fontToolBar->addActions(docu->actions());

    connect(docu,SIGNAL(triggered(QAction*)),this,SLOT(SetAlignment(QAction*)));
}

void MainWindow::createCentral(){
    text = new QTextEdit();
    layout = new QHBoxLayout(this);
   // pFileWatcher = new QFileSystemWatcher();
    QScrollBar *scrollBar = new QScrollBar;
    text->setFixedWidth(750);
    layout->addStretch();
    layout->addWidget(text);
    layout->addStretch();

    layout->addWidget(scrollBar);

    layout->setContentsMargins(0, 0, 0, 0); // 设置布局的边距为0

    // 获取QTextEdit控件的垂直滚动条
    QScrollBar *textScrollBar = text->verticalScrollBar();

    // 设置滚动条的各种属性
    scrollBar->setRange(textScrollBar->minimum(), textScrollBar->maximum());
    scrollBar->setPageStep(textScrollBar->pageStep());
    scrollBar->setValue(textScrollBar->value());
    scrollBar->setSingleStep(textScrollBar->singleStep());
    scrollBar->setSliderPosition(textScrollBar->sliderPosition());
    // 隐藏QTextEdit控件的垂直滚动条
    text->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // 监听文本
    connect(text, &QTextEdit::textChanged, [=]() {
        // 设置滚动条的各种属性
        scrollBar->setRange(textScrollBar->minimum(), textScrollBar->maximum());
        scrollBar->setPageStep(textScrollBar->pageStep());
        scrollBar->setValue(textScrollBar->value());
        scrollBar->setSingleStep(textScrollBar->singleStep());
        scrollBar->setSliderPosition(textScrollBar->sliderPosition());
    });

    connect(text, &QTextEdit::textChanged, [=]() {
       QString wtitle = this->windowTitle();
       if(wtitle[wtitle.size()-1] != '*')
         wtitle = wtitle + "*";
       this->setWindowTitle(wtitle);
    });

    connect(textScrollBar, &QScrollBar::valueChanged, [=](){
        scrollBar->setRange(textScrollBar->minimum(), textScrollBar->maximum());
        scrollBar->setPageStep(textScrollBar->pageStep());
        scrollBar->setValue(textScrollBar->value());
        scrollBar->setSingleStep(textScrollBar->singleStep());
        scrollBar->setSliderPosition(textScrollBar->sliderPosition());
    });



    // 监听滚动条的值的变化
    connect(scrollBar, &QScrollBar::valueChanged, [=](int value) {
        // 设置QTextEdit控件的垂直滚动条的值
        textScrollBar->setValue(value);
    });

    this->centralWidget()->setLayout(layout);
}

void MainWindow::SetFontComboBox(QString Combo){
    QTextCharFormat fmt;
    fmt.setFontFamily(Combo);
    SetTextFormat(fmt);
}

void MainWindow::SetSizeComboBox(QString Value){
    QTextCharFormat fmt;
    fmt.setFontPointSize(Value.toFloat());
    text->mergeCurrentCharFormat(fmt);
}

void MainWindow::SetBold(){
    QTextCharFormat fmt;
    if(boldBtn->isChecked()){
        fmt.setFontWeight(QFont::Bold);
    }else{
        fmt.setFontWeight(QFont::Normal);
    }
    text->mergeCurrentCharFormat(fmt);
}

void MainWindow::SetItalic(){
    QTextCharFormat fmt;
    fmt.setFontItalic(italicBtn->isChecked());
    text->mergeCurrentCharFormat(fmt);
}

void MainWindow::SetUnderline(){
    QTextCharFormat fmt;
    fmt.setFontUnderline(underlineBtn->isChecked());
    text->mergeCurrentCharFormat(fmt);
}

void MainWindow::SetColor(){
    QTextCharFormat fmt;
    QColor color = QColorDialog::getColor(Qt::red,this);
    if(color.isValid()){
        QTextCharFormat fmt;
        fmt.setForeground(color);
        text->mergeCurrentCharFormat(fmt);
    }
}

void MainWindow::SetAlignment(QAction *act){
    if(act == leftAction)           text->setAlignment(Qt::AlignLeft);
    else if(act == rightAction)     text->setAlignment(Qt::AlignRight);
    else if(act == centerAction)    text->setAlignment(Qt::AlignHCenter);
    else                            text->setAlignment(Qt::AlignJustify);
}

void MainWindow::onCursorPositionChanged(){ //刷新工具栏
     QTextCursor cursor = text->textCursor();
     QTextCharFormat fmt = cursor.charFormat();
     // 字体
     fontComboBox->setCurrentFont(fmt.font());
     // 字号
     sizeComboBox->setCurrentText(QString::number(fmt.fontPointSize()));
     // 是否 加粗
     boldBtn->setChecked(fmt.font().bold());
     // 是否 斜体
     italicBtn->setChecked(fmt.font().italic());
     // 是否 下划线
     underlineBtn->setChecked(fmt.font().underline());

     switch(text->alignment()){
        case Qt::AlignLeft:
            leftAction->setChecked(true);
            break;
        case Qt::AlignRight:
            rightAction->setChecked(true);
            break;
        case Qt::AlignHCenter:
            centerAction->setChecked(true);
            break;
        default:
            balanceAction->setChecked(true);
            break;
     }
}

void MainWindow::showFindText()
{
    QString str = findLineEdit->text();
    QPalette palette = text->palette();
    palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
    text->setPalette(palette);
    if (!text->find(str,QTextDocument::FindBackward))
    {
        QMessageBox::warning(this, tr("查找"),tr("找不到%1").arg(str));
    }
}

void MainWindow::showFindText2()
{
    QString str = findLineEdit->text();
    QPalette palette = text->palette();
    palette.setColor(QPalette::Highlight,palette.color(QPalette::Active,QPalette::Highlight));
    text->setPalette(palette);
    if (! text->find(str))
    {
        QMessageBox::warning(this, tr("查找"),tr("找不到%1").arg(str));
    }
}


void MainWindow::on_action_Find_triggered()
{
    QTextCursor cursor = text->textCursor();
    cursor.movePosition(QTextCursor::End);
    findDlg = new QDialog(this);
    findDlg->setWindowTitle(tr("查找与替换"));
    findLineEdit = new QLineEdit(findDlg);
    replacelineedit = new QLineEdit(findDlg);
    QPushButton *btn= new QPushButton(tr("查找上一个"), findDlg);
    QPushButton *btn2= new QPushButton(tr("查找下一个"), findDlg);
    QPushButton *replace=new QPushButton(tr("替换这个"), findDlg);
    QVBoxLayout *layout= new QVBoxLayout(findDlg);
    layout->addWidget(findLineEdit);
    layout->addWidget(btn);
    layout->addWidget(btn2);
    layout->addWidget(replacelineedit);
    layout->addWidget(replace);
    connect(btn, SIGNAL(clicked()), this, SLOT(showFindText()));
    connect(btn2, SIGNAL(clicked()), this, SLOT(showFindText2()));
    connect(replace,SIGNAL(clicked()),this,SLOT(replacetext()));
    findDlg->show();
}

void MainWindow::replacetext()
{
    QString rep=replacelineedit->text();
    QString str = findLineEdit->text();
    QTextCursor cursor = text->textCursor();
    if(! cursor.hasSelection()){ //如果没有选择区域
        QMessageBox::warning(this, tr("查找"),tr("找不到%1").arg(str));
        return;
    }
    cursor.beginEditBlock();
    cursor.insertText(rep);
    cursor.endEditBlock();
    if (! text->find(str))
    {
        QMessageBox::warning(this, tr("查找"),tr("找不到%1").arg(str));
    }
}


void MainWindow::SetTextFormat(QTextCharFormat fmt){
    QTextCursor cursor = text->textCursor(); //获取文本框的光标
    if(! cursor.hasSelection()){ //如果没有选择区域
        cursor.select(QTextCursor::WordUnderCursor);
    }
    cursor.mergeCharFormat(fmt);
    text->mergeCurrentCharFormat(fmt);
}

void MainWindow::on_actionOpen_triggered()
{
    //QString curPath = QDir::currentPath();
    QString filter = "文本文件(*.txt)";
    QString fileName = QFileDialog::getOpenFileName(this,"打开文本文件","../",filter);
    if(fileName.isEmpty())
        return;
    this->opfilename = fileName;
    QString curFileName = "当前文件:" + fileName;
    this->setStatusTip(curFileName);
    this->setWindowTitle(fileName+"-Text Editor");

    QFile file(fileName);
    if(file.open(QIODevice::ReadWrite)){
        text->setText(file.readAll());
        file.close();
    }
}

void MainWindow::on_actionSave_triggered()
{
    QString curPath = QDir::currentPath();
    QString filter = "文本文件(*.txt)";
    QString fileName = QFileDialog::getSaveFileName(this,"另存为",curPath,filter);
    if(fileName.isEmpty())
        return;

    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly)){
        QString content = text->toPlainText();
        QByteArray strBytes = content.toUtf8();
        file.write(strBytes,strBytes.length());
    }
    refresht();
}


void MainWindow::on_actionSave1_triggered()
{
    if(this->opfilename.isEmpty())
    {
        QString curPath = QDir::currentPath();
        QString filter = "文本文件(*.txt)";
        QString fileName = QFileDialog::getSaveFileName(this,"保存",curPath,filter);
        if(fileName.isEmpty())
            return;
    }
    QFile file(this->opfilename);
    if(file.open(QIODevice::WriteOnly)){
        QString content = text->toPlainText();
        QByteArray strBytes = content.toUtf8();
        file.write(strBytes,strBytes.length());
    }
    refresht();
}
bool MainWindow::canBeSaveOrNot(){
    //判断文件是否被修改
    QString wtitle = this->windowTitle();
    if(wtitle[wtitle.size()-1] == '*'){
        QMessageBox::StandardButtons result = QMessageBox::warning(this,"Waring","你想将更改保存吗?",
                    QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        //取消
        if(QMessageBox::Cancel == result){
            return false;
        }
        //保存
        if(QMessageBox::Save == result){
            on_actionSave1_triggered();
            return true;
        }
        //不保存
        if(QMessageBox::Discard == result){
            return true;
        }
    }
    return true;
}

void MainWindow::on_actionNew_triggered()
{
     QString wtitle = this->windowTitle();
     if(canBeSaveOrNot())
      refresha();
}

void MainWindow::on_actionCreateAct1_triggered()
{
    MainWindow *w1 = new MainWindow;
    w1->show();
}

void MainWindow::refresha()
{
     this->setWindowTitle("未命名-Text Editor");
     text->setText("");
}

void MainWindow::refresht()
{
    QString wtitle = this->windowTitle();
    if(wtitle[wtitle.size()-1] == '*')
      wtitle = wtitle.left(wtitle.size()-1);
    this->setWindowTitle(wtitle);
}

void MainWindow::on_actionundo_triggered()
{
    text->undo();
}

void MainWindow::on_actionredo_triggered()
{
    text->redo();
}

void MainWindow::on_actioncut_triggered()
{
    text->cut();
}

void MainWindow::on_actioncopy_triggered()
{
    text->copy();
}

void MainWindow::on_actionpatse_triggered()
{
    text->paste();
}

void MainWindow::on_actiondelete_triggered()
{
    text->textCursor().removeSelectedText();
}


MainWindow::~MainWindow()
{
    delete ui;
}

